%feature("docstring") gum::BNdistance::compute
"
Returns
-------
dict
	a dictionnary containing the different values after the computation.
"