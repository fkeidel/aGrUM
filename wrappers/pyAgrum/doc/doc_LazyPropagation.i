%feature("docstring") gum::LazyPropagation
"
Class used for Lazy Propagation

LazyPropagation(bn) -> LazyPropagation
    Parameters:
        * **bn** (*pyAgrum.BayesNet*) -- a Bayesian network
"